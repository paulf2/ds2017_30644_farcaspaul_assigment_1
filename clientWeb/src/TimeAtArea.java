
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import bllcontrollers.RequestController;
import entities.DepartureCity;
import entities.Flight;

/**
 * Servlet implementation class TimeAtArea
 */
@WebServlet("/TimeAtArea")
public class TimeAtArea extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private RequestController requestController;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TimeAtArea() {
		super();
		requestController = new RequestController();

		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("user")) {
					username = cookie.getValue();
				}
			}
		}
		System.out.println("AICIIIC  " + username);
		if (username == null) {
			response.sendRedirect("login.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	private String getTime(int idcity) {
		String result = "";
		try {
			DepartureCity departureCity = requestController.departureCitiesController.getDepartureCity(idcity);
			String url = "http://www.new.earthtools.org/timezone/" + departureCity.getLatitude() + "/"
					+ departureCity.getLongitude();
			System.out.println(url);
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			int responseCode = con.getResponseCode();
			System.out.println("Response Code : " + responseCode);
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
					.parse(new InputSource(new StringReader(response.toString())));
			NodeList errNodes = doc.getElementsByTagName("timezone");
			if (errNodes.getLength() > 0) {
				Element err = (Element) errNodes.item(0);
				result = err.getElementsByTagName("localtime").item(0).getTextContent();
			} else {
				// success
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return result;
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		int cityId = Integer.parseInt(request.getParameter("idcity"));
		String username = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("user")) {
					username = cookie.getValue();
				}
			}
		}
		if(username==null) {
			response.sendRedirect("login.jsp");
		}
		response.setContentType("text/html");
		response.getWriter().println(getUserPage(username,getTime(cityId)));

	}

	private String getUserPage(String user, String time) {
		String pagina = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n"
				+ "<html>\n" + "<head>\n" + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n"
				+ "<title>User</title>\n" + "</head>\n" + "<body>\n" + "	<h3>\n" + "		Hi " + user
				+ ", Login successful.\n" + "	</h3>\n" + "	<h3>Flights Information</h3>\n" + "	<table>\n"
				+ "		<tr>";
		ArrayList<Flight> flights = requestController.flightsController.getAllFlights();
		if (flights != null)
			for (Flight flight : flights) {
				pagina += "Id: " + flight.getIdflight();
				pagina += "<br/>";
				pagina += "Airplane type: " + flight.getAirplanetype();
				pagina += "<br/>";
				pagina += "Id departure city: " + flight.getIddeparturecity();
				pagina += "<br/>";
				pagina += "Id arrival city: " + flight.getIdarrivalcity();
				pagina += "<br/>";
				pagina += "Departure date: " + flight.getDeparturedate();
				pagina += "<br/>";
				pagina += "Arrival date: " + flight.getArrivaldate();

				pagina += "<br/>";
				pagina += "<br/>";
			}
		pagina += "<td></td>\n" + "			<td>\n" + "				<h3>Departure cities Information</h3>";
		ArrayList<DepartureCity> departureCities = (ArrayList<DepartureCity>) requestController.departureCitiesController
				.getAllDepartureCitys();
		if (departureCities != null)
			for (DepartureCity departureCity : departureCities) {
				pagina += "Id: " + departureCity.getIddeparturecity();
				pagina += "<br/>";
				pagina += "Name: " + departureCity.getName();
				pagina += "<br/>";
				pagina += "Latitude: " + departureCity.getLatitude();
				pagina += "<br/>";
				pagina += "Longitude: " + departureCity.getLongitude();

				pagina += "<br/>";
				pagina += "<br/>";
			}
		pagina += "</td>\n" + "		</tr>\n" + "	</table>\n" + "	<br>\n"
				+ "	<form method=\"post\" action=\"TimeAtArea\">\n" + "		<table>\n" + "			<tr>\n"
				+ "				<td>City id</td>\n" + "				<td><input type=\"text\" name=\"idcity\"></td>\n"
				+ "			</tr>\n" + "			<tr>\n"
				+ "				<td><input type=\"submit\" name=\"action\" value=\"Check\"></td>\n" + "			</tr>\n"
				+ "		</table>\n" + "	</form>\n" + "	<h3>Time at location</h3>\n" + time
				+ "	<form action=\"Logout\" method=\"post\">\n" + "		<input type=\"submit\" value=\"Logout\">\n"
				+ "	</form>\n" + "</body>\n" + "</html>";
		return pagina;
	}
}
