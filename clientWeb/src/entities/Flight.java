package entities;

public class Flight {
	private int idflight;
	private String airplanetype;
	private int iddeparturecity;
	private int idarrivalcity;
	private String departuredate;
	private String arrivaldate;

	public int getIdflight() {
		return idflight;
	}

	public void setIdflight(int idflight) {
		this.idflight = idflight;
	}

	public String getAirplanetype() {
		return airplanetype;
	}

	public void setAirplanetype(String airplanetype) {
		this.airplanetype = airplanetype;
	}

	public int getIddeparturecity() {
		return iddeparturecity;
	}

	public void setIddeparturecity(int iddeparturecity) {
		this.iddeparturecity = iddeparturecity;
	}

	public String getDeparturedate() {
		return departuredate;
	}

	public void setDeparturedate(String departuredate) {
		this.departuredate = departuredate;
	}

	public String getArrivaldate() {
		return arrivaldate;
	}

	public void setArrivaldate(String arrivaldate) {
		this.arrivaldate = arrivaldate;
	}

	public String toString() {
		return "Flight\n		idflight=" + idflight + "\n		airplanetype=" + airplanetype
				+ "\n		iddeparturecity=" + iddeparturecity + "\n		idarrivalcity=" + idarrivalcity
				+ "\n		departuredate=" + departuredate + "\n		arrivaldate=" + arrivaldate;
	}

	public int getIdarrivalcity() {
		return idarrivalcity;
	}

	public void setIdarrivalcity(int idarrivalcity) {
		this.idarrivalcity = idarrivalcity;
	}
}
