package bllcontrollers;
import org.hibernate.cfg.Configuration;

import dao.UserDAO;
import entities.User;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems,
 *          http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description: Controller for the interface elements of the client.
 */
public class UsersController {
	private UserDAO usersDAO;

	public UsersController() {
		usersDAO = new UserDAO(new Configuration().configure("/resources/hibernate.cfg.xml").buildSessionFactory());
	}

	/**
	 * Provides functionality for the POST button.
	 */
	public int postUser(User user) {
		usersDAO.addUser(user);
		return user.getIduser();
	}

	public User updateUser(int iduser, User user) {
		return usersDAO.updateUser(iduser, user);
	}
	
	/**
	 * Provides functionality for the GET button.
	 */
	public User getUser(int iduser) {
		return usersDAO.findUser(iduser);
	}
	
	public boolean isUserValid(User user) {
		ArrayList<User> users = getAllUsers();
		Iterator<User> it= users.iterator();
		while (it.hasNext()) {
			User tempUser = it.next();
			if (tempUser.getUsername().equals(user.getUsername()) && tempUser.getPassword().equals(user.getPassword())) {
				return true;
			}
		}
		return false;
	}
	
	public ArrayList<User> getAllUsers() {
		return (ArrayList<User>) usersDAO.findUsers();
	}
	

	public User deleteUser(int iduser) {
		return usersDAO.deleteUser(iduser);
	}
}
