package bllcontrollers;

import org.hibernate.cfg.Configuration;

import dao.DepartureCityDAO;
import entities.DepartureCity;

import java.util.ArrayList;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems,
 *          http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description: Controller for the interface elements of the client.
 */
public class DepartureCitiesController {
	private DepartureCityDAO departureCityDAO;
	
	public DepartureCitiesController() {
		departureCityDAO = new DepartureCityDAO(new Configuration().configure("/resources/hibernate.cfg.xml").buildSessionFactory());
	}

	/**
	 * Provides functionality for the POST button.
	 */
	public int postDepartureCity(DepartureCity departureCity) {
		departureCityDAO.addDepartureCity(departureCity);
		return departureCity.getIddeparturecity();
	}

	public DepartureCity updateDepartureCity(int iddepartureCity, DepartureCity departureCity) {
        departureCityDAO.updateDepartureCity(iddepartureCity, departureCity);
        return departureCity;
	}
	
	/**
	 * Provides functionality for the GET button.
	 */
	public DepartureCity getDepartureCity(int iddepartureCity) {
        DepartureCity departureCity = departureCityDAO.findDepartureCity(iddepartureCity);
        return departureCity;
	}
	
	public ArrayList<DepartureCity> getAllDepartureCitys() {
		return (ArrayList<DepartureCity>) departureCityDAO.findDepartureCitys();
	}
	

	public DepartureCity deleteDepartureCity(int iddepartureCity) {
		return departureCityDAO.deleteDepartureCity(iddepartureCity);
	}
}
