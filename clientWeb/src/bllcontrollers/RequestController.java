package bllcontrollers;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems,
 *          http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description: Controller for the interface elements of the client.
 */
public class RequestController {
	public DepartureCitiesController departureCitiesController;
	public UsersController usersController;
	public FlightsController flightsController;

	public RequestController() {
		this.departureCitiesController = new DepartureCitiesController();
		this.usersController = new UsersController();
		this.flightsController = new FlightsController();
	}
}
