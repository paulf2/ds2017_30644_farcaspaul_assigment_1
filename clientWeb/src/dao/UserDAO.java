package dao;

import entities.User;
import org.hibernate.*;

import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems,
 *          http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-server
 * @Since: Sep 1, 2015
 * @Description: Uses Hibernate for CRUD operations on the underlying database.
 *               The Hibernate configuration files can be found in the
 *               src/main/resources folder
 */
public class UserDAO {

	private SessionFactory factory;

	public UserDAO(SessionFactory factory) {
		this.factory = factory;
	}

	public User addUser(User user) {
		int userId = -1;
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			userId = (Integer) session.save(user);
			user.setIduser(userId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return user;
	}

	@SuppressWarnings("unchecked")
	public List<User> findUsers() {
		Session session = factory.openSession();
		Transaction tx = null;
		List<User> users = null;
		try {
			tx = session.beginTransaction();
			users = session.createQuery("FROM User").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return users;
	}

	@SuppressWarnings("unchecked")
	public User deleteUser(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<User> users = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM User WHERE iduser = :id");
			query.setParameter("id", id);
			users = query.list();
			tx.commit();

			if (users != null && !users.isEmpty()) {
				try {
					tx = session.beginTransaction();
					session.delete(users.get(0));
					tx.commit();
				} catch (HibernateException e) {
					if (tx != null) {
						tx.rollback();
					}
				}
			}

		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return users != null && !users.isEmpty() ? users.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public User findUser(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<User> users = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM User WHERE iduser = :id");
			query.setParameter("id", id);
			users = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return users != null && !users.isEmpty() ? users.get(0) : null;
	}
	
	public User updateUser(int id, User user) {
		Session session = factory.openSession();
	      Transaction tx = null;
	      try{
	         tx = session.beginTransaction();
	         User oldUser = 
	                    (User)session.get(User.class, id); 
	         oldUser = user;
	         session.clear();
	         session.update(oldUser); 
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	      }finally {
	         session.close(); 
	      }
	      return user;
	}
}
