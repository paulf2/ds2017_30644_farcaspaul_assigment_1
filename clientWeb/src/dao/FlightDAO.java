package dao;

import entities.Flight;
import org.hibernate.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems,
 *          http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-server
 * @Since: Sep 1, 2015
 * @Description: Uses Hibernate for CRUD operations on the underlying database.
 *               The Hibernate configuration files can be found in the
 *               src/main/resources folder
 */
public class FlightDAO {

	private SessionFactory factory;

	public FlightDAO(SessionFactory factory) {
		this.factory = factory;
	}

	public Flight addFlight(Flight flight) {
		int flightId = -1;
		Session session = factory.openSession();
		Transaction tx = null;
		System.out.println("HERE = "+flight);
		try {
			tx = session.beginTransaction();
			flightId = (Integer) session.save(flight);
			flight.setIdflight(flightId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return flight;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Flight> findFlights() {
		Session session = factory.openSession();
		Transaction tx = null;
		ArrayList<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			flights = (ArrayList<Flight>) session.createQuery("FROM Flight").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return flights;
	}

	@SuppressWarnings("unchecked")
	public Flight deleteFlight(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE idflight = :id");
			query.setParameter("id", id);
			flights = query.list();
			tx.commit();

			if (flights != null && !flights.isEmpty()) {
				try {
					tx = session.beginTransaction();
					session.delete(flights.get(0));
					tx.commit();
				} catch (HibernateException e) {
					if (tx != null) {
						tx.rollback();
					}
				}
			}

		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return flights != null && !flights.isEmpty() ? flights.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public Flight findFlight(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE idflight = :id");
			query.setParameter("id", id);
			flights = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			
		} finally {
			session.close();
		}
		return flights != null && !flights.isEmpty() ? flights.get(0) : null;
	}
	
	public Flight updateFlight(int id, Flight flight) {
		Session session = factory.openSession();
	      Transaction tx = null;
	      try{
	         tx = session.beginTransaction();
	         Flight oldFlight = 
	                    (Flight)session.get(Flight.class, id); 
	         oldFlight = flight;
	         session.clear();
	         session.update(oldFlight); 
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	      }finally {
	         session.close(); 
	      }
	      return flight;
	}
}
