
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bllcontrollers.RequestController;
import entities.DepartureCity;
import entities.Flight;

/**
 * Servlet implementation class AddFlight
 */
@WebServlet("/CRUDFlight")
public class CRUDFlight extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private RequestController requestController;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CRUDFlight() {
		super();
		requestController = new RequestController();

		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("admin")) {
					username = cookie.getValue();
				}
			}
		}
		if (username == null) {
			response.sendRedirect("login.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("admin")) {
					username = cookie.getValue();
				}
			}
		}
		if (username == null) {
			response.sendRedirect("login.jsp");
		}
		String action = request.getParameter("action");
		Flight flight = new Flight();
		if (action.equals("remove")) {
			if (request.getParameter("idflight") != null) {
				requestController.flightsController.deleteFlight(Integer.parseInt(request.getParameter("idflight")));
			}
		}
		if (action.equals("add")) {
			if (!(((String) request.getParameter("airplanetype")).equals(""))) {
				flight.setAirplanetype(request.getParameter("airplanetype"));
			}
			if (request.getParameter("iddeparturecity") != null) {
				flight.setIddeparturecity(Integer.parseInt(request.getParameter("iddeparturecity")));
			}
			if (!(((String) request.getParameter("departuredate")).equals(""))) {
				flight.setDeparturedate(request.getParameter("departuredate"));
			}
			if (!(((String) request.getParameter("arrivaldate")).equals(""))) {
				flight.setArrivaldate(request.getParameter("arrivaldate"));
			}
			requestController.flightsController.postFlight(flight);
		}
		if (action.equals("update")) {
			if (request.getParameter("idflight") != null) {
				flight.setIdflight(Integer.parseInt(request.getParameter("idflight")));
			}
			if (!(((String) request.getParameter("airplanetype")).equals(""))) {
				flight.setAirplanetype(request.getParameter("airplanetype"));
			}
			if (request.getParameter("iddeparturecity") != null) {
				flight.setIddeparturecity(Integer.parseInt(request.getParameter("iddeparturecity")));
			}
			if (!(((String) request.getParameter("departuredate")).equals(""))) {
				flight.setDeparturedate(request.getParameter("departuredate"));
			}
			if (!(((String) request.getParameter("arrivaldate")).equals(""))) {
				flight.setArrivaldate(request.getParameter("arrivaldate"));
			}
			requestController.flightsController.updateFlight(flight.getIdflight(), flight);
		}

		response.setContentType("text/html");
		response.getWriter().println(getAdminPage());
	}

	private String getAdminPage() {
		String pagina = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n"
				+ "<html>\n" + "<head>\n" + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n"
				+ "<title>Admin</title>\n" + "</head>\n" + "<body>\n" + "	<h3>\n" + "		Hi\n"
				+ "		admin, Login successful.\n" + "	</h3>\n" + "	<form method=\"post\" action=\"CRUDFlight\">\n"
				+ "		<table>\n" + "			<tr>\n" + "				<td>Flight id</td>\n"
				+ "				<td><input type=\"text\" name=\"idflight\"></td>\n" + "			</tr>\n"
				+ "			<tr>\n" + "				<td>Airplane type</td>\n"
				+ "				<td><input type=\"text\" name=\"airplanetype\"></td>\n" + "			</tr>\n"
				+ "			<tr>\n" + "				<td>Departure city id</td>\n"
				+ "				<td><input type=\"text\" name=\"iddeparturecity\"></td>\n" + "			</tr>\n"
				+ "			<tr>\n" + "				<td>Arrival city id</td>\n"
				+ "				<td><input type=\"text\" name=\"idarrivalcity\"></td>\n" + "			</tr>\n"
				+ "			<tr>\n" + "				<td>Departure date</td>\n"
				+ "				<td><input type=\"text\" name=\"departuredate\"></td>\n" + "			</tr>\n"
				+ "			<tr>\n" + "				<td>Arrival date</td>\n"
				+ "				<td><input type=\"text\" name=\"arrivaldate\"></td>\n" + "			</tr>\n"
				+ "			<tr>\n" + "				<td><input type=\"submit\" name=\"action\" value=\"add\"></td>\n"
				+ "				<td><input type=\"submit\" name=\"action\" value=\"update\"></td>\n"
				+ "				<td><input type=\"submit\" name=\"action\" value=\"remove\"></td>\n"
				+ "			</tr>\n" + "\n" + "		</table>\n" + "	</form>\n" + "\n"
				+ "	<h3>Flights Information</h3>\n" + "	<table>\n" + "		<tr>";

		ArrayList<Flight> flights = requestController.flightsController.getAllFlights();
		if (flights != null)
			for (Flight flight : flights) {
				pagina += "Id: " + flight.getIdflight();
				pagina += "<br/>";
				pagina += "Airplane type: " + flight.getAirplanetype();
				pagina += "<br/>";
				pagina += "Id departure city: " + flight.getIddeparturecity();
				pagina += "<br/>";
				pagina += "Id arrival city: " + flight.getIdarrivalcity();
				pagina += "<br/>";
				pagina += "Departure date: " + flight.getDeparturedate();
				pagina += "<br/>";
				pagina += "Arrival date: " + flight.getArrivaldate();

				pagina += "<br/>";
				pagina += "<br/>";
			}

		pagina += "<td></td>\n" + "			<td>\n" + "				<h3>Departure cities Information</h3>";
		ArrayList<DepartureCity> departureCities = (ArrayList<DepartureCity>) requestController.departureCitiesController
				.getAllDepartureCitys();
		if (departureCities != null)
			for (DepartureCity departureCity : departureCities) {
				pagina += "Id: " + departureCity.getIddeparturecity();
				pagina += "<br/>";
				pagina += "Name: " + departureCity.getName();
				pagina += "<br/>";
				pagina += "Latitude: " + departureCity.getLatitude();
				pagina += "<br/>";
				pagina += "Longitude: " + departureCity.getLongitude();

				pagina += "<br/>";
				pagina += "<br/>";
			}
		pagina += "</td>\n" + "		</tr>\n" + "	</table>\n" + "	<form action=\"Logout\" method=\"post\">\n"
				+ "		<input type=\"submit\" value=\"Logout\">\n" + "	</form>\n" + "</body>\n" + "</html>";
		return pagina;
	}
}
