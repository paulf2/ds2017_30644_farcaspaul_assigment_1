package bllcontrollers;

import org.hibernate.cfg.Configuration;

import dao.FlightDAO;
import entities.Flight;

import java.util.ArrayList;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems,
 *          http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description: Controller for the interface elements of the client.
 */
public class FlightsController {
	private FlightDAO flightsDAO;

	public FlightsController() {
		flightsDAO = new FlightDAO(new Configuration().configure("/resources/hibernate.cfg.xml").buildSessionFactory());
	}

	/**
	 * Provides functionality for the POST button.
	 */
	public int postFlight(Flight flight) {
		flightsDAO.addFlight(flight);
		return flight.getIdflight();
	}

	public Flight updateFlight(int idflight, Flight flight) {
		flightsDAO.updateFlight(idflight, flight);
		return flight;
	}
	
	/**
	 * Provides functionality for the GET button.
	 */
	public Flight getFlight(int idflight) {
		return flightsDAO.findFlight(idflight);	
	}
	
	public ArrayList<Flight> getAllFlights() {
		return flightsDAO.findFlights();
	}
	

	public Flight deleteFlight(int idflight) {
		return flightsDAO.deleteFlight(idflight);
	}
}
