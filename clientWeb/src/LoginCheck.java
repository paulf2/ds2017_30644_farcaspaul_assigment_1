
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bllcontrollers.RequestController;
import entities.DepartureCity;
import entities.Flight;
import entities.User;

/**
 * Servlet implementation class LoginCheck
 */
@WebServlet("/LoginCheck")
public class LoginCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private RequestController requestController;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginCheck() {
		super();
		requestController = new RequestController();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		Cookie passwordCookie = new Cookie("password", password);
		passwordCookie.setMaxAge(30 * 60);
		if (username.equals("admin") && password.equals("admin")) {
			Cookie adminCookie = new Cookie("admin", username);
			adminCookie.setMaxAge(30 * 60);
			response.addCookie(adminCookie);
			response.addCookie(passwordCookie);
			response.setContentType("text/html");
			response.getWriter().println(getAdminPage());
		} else {
			User tempUser = new User();
			tempUser.setUsername(username);
			tempUser.setPassword(password);
			if (requestController.usersController.isUserValid(tempUser)) {
				Cookie userNameCookie = new Cookie("user", username);
				userNameCookie.setMaxAge(30 * 60);
				response.addCookie(userNameCookie);
				response.addCookie(passwordCookie);
				response.setContentType("text/html");
				response.getWriter().println(getUserPage(username));
			} else {
				String pagina = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n"
						+ "<html>\n" + "<head>\n"
						+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n"
						+ "<title>Login Error</title>\n" + "</head>\n" + "<body>\n" + "User invalid.\n" + "</body>\n"
						+ "</html>";
				response.setContentType("text/html");
				response.getWriter().println(pagina);

			}
		}
	}

	private String getUserPage(String user) {
		String pagina = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n"
				+ "<html>\n" + "<head>\n" + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n"
				+ "<title>User</title>\n" + "</head>\n" + "<body>\n" + "	<h3>\n" + "		Hi " + user
				+ ", Login successful.\n" + "	</h3>\n" + "	<h3>Flights Information</h3>\n" + "	<table>\n"
				+ "		<tr>";
		ArrayList<Flight> flights = requestController.flightsController.getAllFlights();
		if (flights != null)
			for (Flight flight : flights) {
				pagina += "Id: " + flight.getIdflight();
				pagina += "<br/>";
				pagina += "Airplane type: " + flight.getAirplanetype();
				pagina += "<br/>";
				pagina += "Id departure city: " + flight.getIddeparturecity();
				pagina += "<br/>";
				pagina += "Id arrival city: " + flight.getIdarrivalcity();
				pagina += "<br/>";
				pagina += "Departure date: " + flight.getDeparturedate();
				pagina += "<br/>";
				pagina += "Arrival date: " + flight.getArrivaldate();

				pagina += "<br/>";
				pagina += "<br/>";
			}
		pagina += "<td></td>\n" + "			<td>\n" + "				<h3>Departure cities Information</h3>";
		ArrayList<DepartureCity> departureCities = (ArrayList<DepartureCity>) requestController.departureCitiesController
				.getAllDepartureCitys();
		if (departureCities != null)
			for (DepartureCity departureCity : departureCities) {
				pagina += "Id: " + departureCity.getIddeparturecity();
				pagina += "<br/>";
				pagina += "Name: " + departureCity.getName();
				pagina += "<br/>";
				pagina += "Latitude: " + departureCity.getLatitude();
				pagina += "<br/>";
				pagina += "Longitude: " + departureCity.getLongitude();

				pagina += "<br/>";
				pagina += "<br/>";
			}
		pagina += "</td>\n" + "		</tr>\n" + "	</table>\n" + "	<br>\n"
				+ "	<form method=\"post\" action=\"TimeAtArea\">\n" + "		<table>\n" + "			<tr>\n"
				+ "				<td>City id</td>\n" + "				<td><input type=\"text\" name=\"idcity\"></td>\n"
				+ "			</tr>\n" + "			<tr>\n"
				+ "				<td><input type=\"submit\" name=\"action\" value=\"Check\"></td>\n" + "			</tr>\n"
				+ "		</table>\n" + "	</form>\n" + "	<h3>Time at location</h3>\n"
				+ "	<form action=\"Logout\" method=\"post\">\n" + "		<input type=\"submit\" value=\"Logout\">\n"
				+ "	</form>\n" + "</body>\n" + "</html>";
		return pagina;
	}

	private String getAdminPage() {
		String pagina = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n"
				+ "<html>\n" + "<head>\n" + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n"
				+ "<title>Admin</title>\n" + "</head>\n" + "<body>\n" + "	<h3>\n" + "		Hi\n"
				+ "		admin, Login successful.\n" + "	</h3>\n" + "	<form method=\"post\" action=\"CRUDFlight\">\n"
				+ "		<table>\n" + "			<tr>\n" + "				<td>Flight id</td>\n"
				+ "				<td><input type=\"text\" name=\"idflight\"></td>\n" + "			</tr>\n"
				+ "			<tr>\n" + "				<td>Airplane type</td>\n"
				+ "				<td><input type=\"text\" name=\"airplanetype\"></td>\n" + "			</tr>\n"
				+ "			<tr>\n" + "				<td>Departure city id</td>\n"
				+ "				<td><input type=\"text\" name=\"iddeparturecity\"></td>\n" + "			</tr>\n"
				+ "			<tr>\n" + "				<td>Arrival city id</td>\n"
				+ "				<td><input type=\"text\" name=\"idarrivalcity\"></td>\n" + "			</tr>\n"
				+ "			<tr>\n" + "				<td>Departure date</td>\n"
				+ "				<td><input type=\"text\" name=\"departuredate\"></td>\n" + "			</tr>\n"
				+ "			<tr>\n" + "				<td>Arrival date</td>\n"
				+ "				<td><input type=\"text\" name=\"arrivaldate\"></td>\n" + "			</tr>\n"
				+ "			<tr>\n" + "				<td><input type=\"submit\" name=\"action\" value=\"add\"></td>\n"
				+ "				<td><input type=\"submit\" name=\"action\" value=\"update\"></td>\n"
				+ "				<td><input type=\"submit\" name=\"action\" value=\"remove\"></td>\n"
				+ "			</tr>\n" + "\n" + "		</table>\n" + "	</form>\n" + "\n"
				+ "	<h3>Flights Information</h3>\n" + "	<table>\n" + "		<tr>";

		ArrayList<Flight> flights = requestController.flightsController.getAllFlights();
		if (flights != null)
			for (Flight flight : flights) {
				pagina += "Id: " + flight.getIdflight();
				pagina += "<br/>";
				pagina += "Airplane type: " + flight.getAirplanetype();
				pagina += "<br/>";
				pagina += "Id departure city: " + flight.getIddeparturecity();
				pagina += "<br/>";
				pagina += "Id arrival city: " + flight.getIdarrivalcity();
				pagina += "<br/>";
				pagina += "Departure date: " + flight.getDeparturedate();
				pagina += "<br/>";
				pagina += "Arrival date: " + flight.getArrivaldate();

				pagina += "<br/>";
				pagina += "<br/>";
			}

		pagina += "<td></td>\n" + "			<td>\n" + "				<h3>Departure cities Information</h3>";
		ArrayList<DepartureCity> departureCities = (ArrayList<DepartureCity>) requestController.departureCitiesController
				.getAllDepartureCitys();
		if (departureCities != null)
			for (DepartureCity departureCity : departureCities) {
				pagina += "Id: " + departureCity.getIddeparturecity();
				pagina += "<br/>";
				pagina += "Name: " + departureCity.getName();
				pagina += "<br/>";
				pagina += "Latitude: " + departureCity.getLatitude();
				pagina += "<br/>";
				pagina += "Longitude: " + departureCity.getLongitude();

				pagina += "<br/>";
				pagina += "<br/>";
			}
		pagina += "</td>\n" + "		</tr>\n" + "	</table>\n" + "	<form action=\"Logout\" method=\"post\">\n"
				+ "		<input type=\"submit\" value=\"Logout\">\n" + "	</form>\n" + "</body>\n" + "</html>";
		return pagina;
	}
}