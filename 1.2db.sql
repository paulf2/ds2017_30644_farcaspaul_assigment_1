CREATE DATABASE  if not exists `flights`;
 

CREATE TABLE if not exists `departurecity`  (
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `name` varchar(45) NOT NULL,
  `iddeparturecity` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`iddeparturecity`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE  if not exists `flight` (
  `idflight` int(11) NOT NULL AUTO_INCREMENT,
  `airplanetype` varchar(45) NOT NULL,
  `iddeparturecity` int(11) NOT NULL,
  `departuredate` datetime NOT NULL,
  `arrivaldate` datetime NOT NULL,
  `idarrivalcity` int(11) NOT NULL,
  PRIMARY KEY (`idflight`),
  UNIQUE KEY `idflight_UNIQUE` (`idflight`),
  KEY `departurecityname_idx` (`iddeparturecity`),
  KEY `idarrivalcity_idx` (`idarrivalcity`),
  CONSTRAINT `iddeparturecity` FOREIGN KEY (`iddeparturecity`) REFERENCES `departurecity` (`iddeparturecity`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE  if not exists `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
