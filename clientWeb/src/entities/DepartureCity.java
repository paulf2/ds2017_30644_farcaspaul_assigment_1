package entities;

public class DepartureCity {
	private int iddeparturecity;
	private String name;
	private double latitude;
	private double longitude;

	public int getIddeparturecity() {
		return iddeparturecity;
	}

	public void setIddeparturecity(int iddeparturecity) {
		this.iddeparturecity = iddeparturecity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String toString() {
		return "DepartureCity\n		iddeparturecity=" + iddeparturecity + "\n		name=" + name + "\n		latitude=" + latitude + "\n		longitude=" + longitude;
	}
}
