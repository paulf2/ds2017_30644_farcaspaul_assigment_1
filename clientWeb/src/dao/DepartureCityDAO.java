package dao;

import entities.DepartureCity;
import org.hibernate.*;

import java.util.List;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems,
 *          http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-server
 * @Since: Sep 1, 2015
 * @Description: Uses Hibernate for CRUD operations on the underlying database.
 *               The Hibernate configuration files can be found in the
 *               src/main/resources folder
 */
public class DepartureCityDAO {

	private SessionFactory factory;

	public DepartureCityDAO(SessionFactory factory) {
		this.factory = factory;
	}

	public DepartureCity addDepartureCity(DepartureCity departureCity) {
		int departureCityId = -1;
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			departureCityId = (Integer) session.save(departureCity);
			departureCity.setIddeparturecity(departureCityId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return departureCity;
	}

	@SuppressWarnings("unchecked")
	public List<DepartureCity> findDepartureCitys() {
		Session session = factory.openSession();
		Transaction tx = null;
		List<DepartureCity> departureCitys = null;
		try {
			tx = session.beginTransaction();
			departureCitys = session.createQuery("FROM DepartureCity").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return departureCitys;
	}

	@SuppressWarnings("unchecked")
	public DepartureCity deleteDepartureCity(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<DepartureCity> departureCitys = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM DepartureCity WHERE iddeparturecity = :id");
			query.setParameter("id", id);
			departureCitys = query.list();
			tx.commit();

			if (departureCitys != null && !departureCitys.isEmpty()) {
				try {
					tx = session.beginTransaction();
					session.delete(departureCitys.get(0));
					tx.commit();
				} catch (HibernateException e) {
					if (tx != null) {
						tx.rollback();
					}
				}
			}

		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return departureCitys != null && !departureCitys.isEmpty() ? departureCitys.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public DepartureCity findDepartureCity(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<DepartureCity> departureCitys = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM DepartureCity WHERE iddeparturecity = :id");
			query.setParameter("id", id);
			departureCitys = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return departureCitys != null && !departureCitys.isEmpty() ? departureCitys.get(0) : null;
	}

	public DepartureCity updateDepartureCity(int id, DepartureCity departureCity) {
		Session session = factory.openSession();
	      Transaction tx = null;
	      try{
	         tx = session.beginTransaction();
	         DepartureCity oldDepartureCity = 
	                    (DepartureCity)session.get(DepartureCity.class, id); 
	         oldDepartureCity = departureCity;
	         session.clear();
	         session.update(oldDepartureCity); 
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	      }finally {
	         session.close(); 
	      }
	      return departureCity;
	}
}
